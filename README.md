# Initialise #

Create a database first and input its name and credentials to it in the `.env` file in the root folder.

Then run these commands in the root folder:

`composer update`

`php artisan migrate`

### Usage ###

Go to `localhost/app_root_folder/public` in your browser

You are now able to populate the database using the `Load / Reload Data` Button.

Everything else is self-explanatory.