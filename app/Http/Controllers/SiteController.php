<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Country;
use App\Language;

class SiteController extends Controller
{
	public function init() {

		$client = new Client();
		$res = $client->request('GET', 'https://restcountries.eu/rest/v1/all');

		$countries 			= json_decode($res->getBody());
		$insert_countries 	= array();
		$insert_langs 		= array();
		$id 				= 0;

		// Prepare queries
		foreach($countries as $country) {

			$id++;
			$name 			= $country->name;
			$tld 			= $country->topLevelDomain[0];
			$a2code 		= $country->alpha2Code;
			$a3code 		= $country->alpha3Code;

			foreach($country->languages as $lang) {
				$insert_langs[] = array(
					'country_id' => $id,
					'lang_code'  => $lang
				);
			}

			$translations 	= '';
			$comma 			= '';
			foreach($country->translations as $c => $t) {
				$translations  .= $comma.$c." : ".$t;
				$comma 			= ', ';
			}

			$insert_countries[] = [
				'name' 			=> $name,
				'tld' 			=> $tld,
				'alpha2_code'	=> $a2code,
				'alpha3_code'	=> $a3code,
				'translations'	=> $translations,
				'coordinates' 	=> isset($country->latlng[0]) ? $country->latlng[0].", ".$country->latlng[1] : 'N/A',
				'borders' 		=> implode(", ",$country->borders)
			];
		}

		// Insert everything
		\DB::table('countries')->truncate();
		\DB::table('country_languages')->truncate();

		\DB::table('countries')->insert($insert_countries);
		\DB::table('country_languages')->insert($insert_langs);

		return redirect()->route('site.index');
	}

	public function index() {
		$languages = Language::all();
		$formatted_langs = [];
		foreach($languages as $lang) {
			if(!isset($formatted_langs[$lang->country_id])) {
				$formatted_langs[$lang->country_id] = " ".$lang->lang_code." ";
			} else {
				$formatted_langs[$lang->country_id] .= $lang->lang_code." ";
			}
		}

		$data['countries'] = Country::all();
		$data['languages'] = $formatted_langs;

		return view('index', $data);
	}

	public function search(Request $request) {
		$type 				= $request->get('type');
		$value 				= $request->get('val');
		$countries 			= [];
		$formatted_langs 	= [];
		$html = '';

		if($type == 'iso') {

			$country = Country::where('alpha2_code', $value)
				->first();

			if(count($country) < 1) {
				return '';
			}

			$languages = Language::where('country_id', $country->id)
				->select(\DB::raw('group_concat(lang_code) as codes'))
				->first();

			$html .= "<tr>";
				$html .= "<td>".$country->name."</td>";
				$html .= "<td>".$country->tld."</td>";
				$html .= "<td>".$country->alpha2_code."</td>";
				$html .= "<td>".$country->alpha3_code."</td>";
				$html .= "<td>".$languages['codes']."</td>";
				$html .= "<td>".$country->translations."</td>";
				$html .= "<td>".$country->coordinates."</td>";
				$html .= "<td>".$country->borders."</td>";		
			$html .= "</tr>";

			return $html;

		} else if($type == 'lang') {

			$temp_country = Language::where('lang_code', $value)
				->get();

			$country_ids = [];
			foreach($temp_country as $id) {
				$country_ids[] = $id->country_id;
			}

			$languages = Language::whereIn('country_id', $country_ids)
				->get();

			$formatted_countries = [];
			foreach($languages as $lang) {
				if(!isset($formatted_countries[$lang->country_id]['languages'])) {
					$formatted_countries[$lang->country_id]['languages'] = " ".$lang->lang_code;
				} else {
					$formatted_countries[$lang->country_id]['languages'] .= ", ".$lang->lang_code;
				}
			}
			
			$countries = Country::whereIn('id', $country_ids)
				->get();
			foreach($countries as $country) {
				$formatted_countries[$country->id]['name'] 			= $country->name;
				$formatted_countries[$country->id]['tld'] 			= $country->tld;
				$formatted_countries[$country->id]['alpha2_code'] 	= $country->alpha2_code;
				$formatted_countries[$country->id]['alpha3_code'] 	= $country->alpha3_code;
				$formatted_countries[$country->id]['translations'] 	= $country->translations;
				$formatted_countries[$country->id]['coordinates'] 	= $country->coordinates;
				$formatted_countries[$country->id]['borders'] 		= $country->borders;
			}

			$html = '';
			foreach($formatted_countries as $c) {
				$html .= "<tr>";
					$html .= "<td>".$c['name']."</td>";
					$html .= "<td>".$c['tld']."</td>";
					$html .= "<td>".$c['alpha2_code']."</td>";
					$html .= "<td>".$c['alpha3_code']."</td>";
					$html .= "<td>".$c['languages']."</td>";
					$html .= "<td>".$c['translations']."</td>";
					$html .= "<td>".$c['coordinates']."</td>";
					$html .= "<td>".$c['borders']."</td>";		
				$html .= "</tr>";
			}

			return $html;
		}	

	return "I'm Batman!";	
	}
}
