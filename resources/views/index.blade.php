<!DOCTYPE html>
<html>
<head>
	<title>SamKnows Test</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
</head>
<body>
	<a href="{{ route('site.init') }}" id="reload-data">Load / Reload Data</a>
	<div id="search-div">
		<input type="text" placeholder="example: af, bv..." /><span data-type="iso" class="search">Search by ISO2</span><br />
		<input type="text" placeholder="example: en, sp..." /><span data-type="lang" class="search">Search by Language</span>
	</div>


	<h1>List of Countries:</h1>
	<table id="dataTable" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
                <th>Name</th>
                <th>First LTD</th>
                <th>ISO2 Code</th>
                <th>ISO3 Code</th>
                <th>Languages</th>
                <th>Translations</th>
                <th>Coordinates</th>
                <th>Bording with</th>
            </tr>
		</thead>
		<tfoot>
			<tr>
                <th>Name</th>
                <th>First LTD</th>
                <th>ISO2 Code</th>
                <th>ISO3 Code</th>
                <th>Languages</th>
                <th>Translations</th>
                <th>Coordinates</th>
                <th>Bording with</th>
            </tr>
		</tfoot>
		<tbody>
			@foreach($countries as $c)
				<tr>
					<td>{{ $c->name }}</td>
					<td>{{ $c->tld }}</td>
					<td>{{ $c->alpha2_code }}</td>
					<td>{{ $c->alpha3_code }}</td>
					<td>{{ isset($languages[$c->id]) ? $languages[$c->id] : "N\A" }}</td>
					<td>{{ $c->translations }}</td>
					<td>{{ $c->coordinates }}</td>
					<td>{{ $c->borders }}</td>
				</tr>
				
			@endforeach
		</tbody>
	</table>

	<script
	  	src="https://code.jquery.com/jquery-3.1.1.min.js"
	  	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	  	crossorigin="anonymous"></script>
	<script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

	<script>

		$( document ).ready(function() {
			var table = $('#dataTable').DataTable({
				"pageLength": 25,
				"lengthChange": false
			});
		
			$(".search").click(function() {
				var thisObj = $(this);
				var type 	= $(this).data("type");
				var value	= $(this).prev().val();

				if(value.length < 2) {
					alert("Value should be at least 2 characters");
				}

				if(!$(this).hasClass("in-progress") && value.length >= 2) {
					$(this).addClass("in-progress");

					$.ajax({
					    url 	: "{{ route('site.search') }}",
					    type	: "GET",
					    data 	: {
					    	type : type,
					    	val  : value
					    }
					})
					.done(function( data ) {
						thisObj.removeClass("in-progress");
						table.destroy();
						$("tbody").html(data);
						table = $('#dataTable').DataTable({
							"pageLength": 25,
							"lengthChange": false
						});
					});
				}
			})
		});
	</script>
</body>
</html>